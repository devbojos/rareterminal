#pragma once

#include "App.hpp"

namespace RareTerminal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ControlPanel
	/// </summary>
	public ref class ControlPanel : public System::Windows::Forms::Form
	{
	private:
		const App* app;
	public:
		ControlPanel(App* app)
		{
			this->app = app;
			InitializeComponent();

			this->ClientSize = System::Drawing::Size(600, 400);
			this->menuStrip1->Size = System::Drawing::Size(600, 24);
			this->appContentPanel->Location = System::Drawing::Point(0, 30);
			this->httpContentPanel->Location = System::Drawing::Point(0, 30);
			this->terminalContentPanel->Location = System::Drawing::Point(0, 30);

			ToTray();


			System::String^ appver = gcnew String(this->app->VERSION);
			System::String^ version("version ");
			this->versionLabel->Text = String::Concat(version, appver);

		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ControlPanel()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::NotifyIcon^ trayIcon;
	private: System::Windows::Forms::MenuStrip^ menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^ appCategory;

	private: System::Windows::Forms::ToolStripMenuItem^ appMinimize;
	private: System::Windows::Forms::ToolStripMenuItem^ appExit;
	private: System::Windows::Forms::ToolStripMenuItem^ terminalCategory;
	private: System::Windows::Forms::ToolStripMenuItem^ httpCategory;
	private: System::Windows::Forms::ToolStripMenuItem^ debugCategory;





	private: System::Windows::Forms::ToolStripMenuItem^ logsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ performanceToolStripMenuItem;


	private: System::Windows::Forms::ToolStripMenuItem^ terminalSettings;



	private: System::Windows::Forms::ToolStripMenuItem^ httpSettings;
	private: System::Windows::Forms::Panel^ appContentPanel;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ versionLabel;
	private: System::Windows::Forms::Panel^ terminalContentPanel;

	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Panel^ httpContentPanel;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label7;





	protected:
	private: System::ComponentModel::IContainer^ components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		/// 

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(ControlPanel::typeid));
			this->trayIcon = (gcnew System::Windows::Forms::NotifyIcon(this->components));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->appCategory = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->appMinimize = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->appExit = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->terminalCategory = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->terminalSettings = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->httpCategory = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->httpSettings = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->debugCategory = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->logsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->performanceToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->appContentPanel = (gcnew System::Windows::Forms::Panel());
			this->versionLabel = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->terminalContentPanel = (gcnew System::Windows::Forms::Panel());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->httpContentPanel = (gcnew System::Windows::Forms::Panel());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1->SuspendLayout();
			this->appContentPanel->SuspendLayout();
			this->terminalContentPanel->SuspendLayout();
			this->httpContentPanel->SuspendLayout();
			this->SuspendLayout();
			// 
			// trayIcon
			// 
			this->trayIcon->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"trayIcon.Icon")));
			this->trayIcon->Text = L"RareTerminal CP";
			this->trayIcon->MouseDoubleClick += gcnew System::Windows::Forms::MouseEventHandler(this, &ControlPanel::trayIcon_MouseDoubleClick);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
				this->appCategory, this->terminalCategory,
					this->httpCategory, this->debugCategory
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1294, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// appCategory
			// 
			this->appCategory->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->appMinimize,
					this->appExit
			});
			this->appCategory->Name = L"appCategory";
			this->appCategory->Size = System::Drawing::Size(41, 20);
			this->appCategory->Text = L"App";
			this->appCategory->Click += gcnew System::EventHandler(this, &ControlPanel::appCategory_Click);
			// 
			// appMinimize
			// 
			this->appMinimize->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->appMinimize->Name = L"appMinimize";
			this->appMinimize->Size = System::Drawing::Size(160, 22);
			this->appMinimize->Text = L"Minimize to tray";
			this->appMinimize->Click += gcnew System::EventHandler(this, &ControlPanel::appMinimize_Click);
			// 
			// appExit
			// 
			this->appExit->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->appExit->Name = L"appExit";
			this->appExit->Size = System::Drawing::Size(160, 22);
			this->appExit->Text = L"Exit";
			this->appExit->Click += gcnew System::EventHandler(this, &ControlPanel::appExit_Click);
			// 
			// terminalCategory
			// 
			this->terminalCategory->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->terminalSettings });
			this->terminalCategory->Name = L"terminalCategory";
			this->terminalCategory->Size = System::Drawing::Size(64, 20);
			this->terminalCategory->Text = L"Terminal";
			this->terminalCategory->Click += gcnew System::EventHandler(this, &ControlPanel::terminalCategory_Click);
			// 
			// terminalSettings
			// 
			this->terminalSettings->Name = L"terminalSettings";
			this->terminalSettings->Size = System::Drawing::Size(116, 22);
			this->terminalSettings->Text = L"Settings";
			// 
			// httpCategory
			// 
			this->httpCategory->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->httpSettings });
			this->httpCategory->Name = L"httpCategory";
			this->httpCategory->Size = System::Drawing::Size(43, 20);
			this->httpCategory->Text = L"Http";
			this->httpCategory->Click += gcnew System::EventHandler(this, &ControlPanel::httpCategory_Click);
			// 
			// httpSettings
			// 
			this->httpSettings->Name = L"httpSettings";
			this->httpSettings->Size = System::Drawing::Size(116, 22);
			this->httpSettings->Text = L"Settings";
			// 
			// debugCategory
			// 
			this->debugCategory->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->logsToolStripMenuItem,
					this->performanceToolStripMenuItem
			});
			this->debugCategory->Name = L"debugCategory";
			this->debugCategory->Size = System::Drawing::Size(54, 20);
			this->debugCategory->Text = L"Debug";
			// 
			// logsToolStripMenuItem
			// 
			this->logsToolStripMenuItem->Name = L"logsToolStripMenuItem";
			this->logsToolStripMenuItem->Size = System::Drawing::Size(142, 22);
			this->logsToolStripMenuItem->Text = L"Logs";
			// 
			// performanceToolStripMenuItem
			// 
			this->performanceToolStripMenuItem->Name = L"performanceToolStripMenuItem";
			this->performanceToolStripMenuItem->Size = System::Drawing::Size(142, 22);
			this->performanceToolStripMenuItem->Text = L"Performance";
			// 
			// appContentPanel
			// 
			this->appContentPanel->BackColor = System::Drawing::Color::Transparent;
			this->appContentPanel->Controls->Add(this->versionLabel);
			this->appContentPanel->Controls->Add(this->label2);
			this->appContentPanel->Controls->Add(this->label1);
			this->appContentPanel->Location = System::Drawing::Point(589, 30);
			this->appContentPanel->Name = L"appContentPanel";
			this->appContentPanel->Size = System::Drawing::Size(583, 334);
			this->appContentPanel->TabIndex = 1;
			this->appContentPanel->Visible = false;
			// 
			// versionLabel
			// 
			this->versionLabel->AutoSize = true;
			this->versionLabel->Font = (gcnew System::Drawing::Font(L"Consolas", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->versionLabel->ForeColor = System::Drawing::Color::White;
			this->versionLabel->Location = System::Drawing::Point(438, 20);
			this->versionLabel->Name = L"versionLabel";
			this->versionLabel->Size = System::Drawing::Size(88, 17);
			this->versionLabel->TabIndex = 2;
			this->versionLabel->Text = L"version {}";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Segoe UI Light", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->ForeColor = System::Drawing::Color::White;
			this->label2->Location = System::Drawing::Point(13, 50);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(142, 21);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Control Panel - App";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(186)), static_cast<System::Int32>(static_cast<System::Byte>(218)),
				static_cast<System::Int32>(static_cast<System::Byte>(85)));
			this->label1->Location = System::Drawing::Point(12, 20);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(141, 30);
			this->label1->TabIndex = 0;
			this->label1->Text = L"RareTerminal";
			// 
			// terminalContentPanel
			// 
			this->terminalContentPanel->BackColor = System::Drawing::Color::Transparent;
			this->terminalContentPanel->Controls->Add(this->label4);
			this->terminalContentPanel->Controls->Add(this->label5);
			this->terminalContentPanel->Location = System::Drawing::Point(0, 370);
			this->terminalContentPanel->Name = L"terminalContentPanel";
			this->terminalContentPanel->Size = System::Drawing::Size(583, 334);
			this->terminalContentPanel->TabIndex = 3;
			this->terminalContentPanel->Visible = false;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Segoe UI Light", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label4->ForeColor = System::Drawing::Color::White;
			this->label4->Location = System::Drawing::Point(13, 50);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(168, 21);
			this->label4->TabIndex = 1;
			this->label4->Text = L"Control Panel - Terminal";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Segoe UI", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(186)), static_cast<System::Int32>(static_cast<System::Byte>(218)),
				static_cast<System::Int32>(static_cast<System::Byte>(85)));
			this->label5->Location = System::Drawing::Point(12, 20);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(141, 30);
			this->label5->TabIndex = 0;
			this->label5->Text = L"RareTerminal";
			// 
			// httpContentPanel
			// 
			this->httpContentPanel->BackColor = System::Drawing::Color::Transparent;
			this->httpContentPanel->Controls->Add(this->label6);
			this->httpContentPanel->Controls->Add(this->label7);
			this->httpContentPanel->Location = System::Drawing::Point(0, 30);
			this->httpContentPanel->Name = L"httpContentPanel";
			this->httpContentPanel->Size = System::Drawing::Size(583, 334);
			this->httpContentPanel->TabIndex = 3;
			this->httpContentPanel->Visible = false;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Segoe UI Light", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label6->ForeColor = System::Drawing::Color::White;
			this->label6->Location = System::Drawing::Point(13, 50);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(144, 21);
			this->label6->TabIndex = 1;
			this->label6->Text = L"Control Panel - Http";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Segoe UI", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label7->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(186)), static_cast<System::Int32>(static_cast<System::Byte>(218)),
				static_cast<System::Int32>(static_cast<System::Byte>(85)));
			this->label7->Location = System::Drawing::Point(12, 20);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(141, 30);
			this->label7->TabIndex = 0;
			this->label7->Text = L"RareTerminal";
			// 
			// ControlPanel
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(22)), static_cast<System::Int32>(static_cast<System::Byte>(22)),
				static_cast<System::Int32>(static_cast<System::Byte>(22)));
			this->ClientSize = System::Drawing::Size(1294, 806);
			this->Controls->Add(this->appContentPanel);
			this->Controls->Add(this->terminalContentPanel);
			this->Controls->Add(this->httpContentPanel);
			this->Controls->Add(this->menuStrip1);
			this->ForeColor = System::Drawing::SystemColors::ControlText;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ControlPanel";
			this->Text = L"RareTerminal CP";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->appContentPanel->ResumeLayout(false);
			this->appContentPanel->PerformLayout();
			this->terminalContentPanel->ResumeLayout(false);
			this->terminalContentPanel->PerformLayout();
			this->httpContentPanel->ResumeLayout(false);
			this->httpContentPanel->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}

#pragma endregion

	public: 
		System::Void ToTray()
		{
			this->ShowInTaskbar = false;
			Hide();
			this->WindowState = System::Windows::Forms::FormWindowState::Minimized;
			this->trayIcon->Visible = true;
		}
		System::Void ToWindow()
		{
			this->ShowInTaskbar = true;
			Show();
			this->WindowState = System::Windows::Forms::FormWindowState::Normal;
			this->trayIcon->Visible = false;
		}

		System::Void HideContent() 
		{
			this->appContentPanel->Visible      = false;
			this->terminalContentPanel->Visible = false;
			this->httpContentPanel->Visible     = false;
		}



	private: 
		System::Void trayIcon_MouseDoubleClick(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e) 
		{
			ToWindow();
		}
		System::Void appMinimize_Click(System::Object^ sender, System::EventArgs^ e) 
		{
			ToTray();
		}
		System::Void appExit_Click(System::Object^ sender, System::EventArgs^ e)
		{
			this->Close();
		}

		System::Void terminalCategory_Click(System::Object^ sender, System::EventArgs^ e)
		{
			HideContent();
			this->terminalContentPanel->Visible = true;
		}
		System::Void httpCategory_Click(System::Object^ sender, System::EventArgs^ e)
		{
			HideContent();
			this->httpContentPanel->Visible = true;
		}
		System::Void appCategory_Click(System::Object^ sender, System::EventArgs^ e) 
		{
			HideContent();
			this->appContentPanel->Visible = true;
		}
};
}
