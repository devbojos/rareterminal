//
// Created by maros on 8/25/2020.
//

#ifndef RARE_TERMINAL_TRANSPAYMENT_HPP
#define RARE_TERMINAL_TRANSPAYMENT_HPP

#include "TransPacket.hpp"

class TransPayment : TransPacket {
private:
    float amount;
    float cashbackAmount;
    const char* variableSymbol;

public:
    /**
     * Default constructor
     * @param amount         Amount
     * @param cashbackAmount Cashback amount
     * @param variableSymbol Variable symbol

     */
    TransPayment(float amount, float cashbackAmount, const char *variableSymbol);

public:
    char *ToBuffer() override;
};


#endif //RARE_TERMINAL_TRANSPAYMENT_HPP
