#include "ControlPanel.h"
#include "App.hpp"

using namespace System;
using namespace System::Windows::Forms;	

using namespace System::Diagnostics;

[STAThreadAttribute]
void Main(array<String^>^ args) {

    Application::EnableVisualStyles();
    Application::SetCompatibleTextRenderingDefault(false);

    App app;

    RareTerminal::ControlPanel form(&app);
    Application::Run(%form);


}