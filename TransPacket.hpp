//
// Created by maros on 8/25/2020.
//

#ifndef RARE_TERMINAL_TRANSPACKET_HPP
#define RARE_TERMINAL_TRANSPACKET_HPP

#include <cstdio>
#include <malloc.h>

class TransPacket {
public:
    const unsigned TRANS_ID;
public:
    const unsigned PROTOCOL_VERSION = 115;

protected:
    /**
     * Default constructor specifying transaction ID
     * @param transId Unique transaction ID that is recognised by Terminal
     */
    explicit TransPacket(unsigned int transId) : TRANS_ID(transId){};

public:
    virtual char* ToBuffer() {
        return nullptr;
    }
};

#endif //RARE_TERMINAL_TRANSPACKET_HPP
