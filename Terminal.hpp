//
// Created by maros on 8/24/2020.
//

#ifndef RARE_TERMINAL_TERMINAL_HPP
#define RARE_TERMINAL_TERMINAL_HPP

#include <winsock2.h>
#include "Packets.hpp"

using namespace System::Threading;
using namespace System::Collections;

class Terminal {

private:
    short socket_family;
    short socket_type;
    short socket_protocol;

private:
    sockaddr_in addr;

    SOCKET sockd;
    WSAData wsaData;

private:
    volatile bool shouldDisconnect;
    volatile bool connected;
    volatile bool processing;

public:
    /**
     * Default constructor
     * @param terminalAddr Address of terminal
     * @param terminalPort Service port of terminal
     */
    Terminal(const char *terminalAddr, unsigned int terminalPort);

    virtual ~Terminal();

    /**
     * Connects to terminal
     * @return Returns true if connection to terminal was successful
     */
    bool Connect();

    /**
     * Disconnects from terminal
     * @return Returns true if disconnect from terminal was successful
     */
    bool Disconnect();
private:
   // bool ProcessPacket(TransPacket request, std::future<RespvPacket> response);


public:
    /**
     * Calculates LRC
     * @return LRC
     */
    static unsigned int CalcLRC(const char* buffer, int offset) {
        unsigned lrc = 0;
        for(int i = offset; ; i++) {
            // fail statement
            if(i == 251) {
                printf("Max byte count reached when calculating LRC\n");
                return lrc;
            }
            unsigned data = buffer[i];
            if(data == 0x0) {
                OutputDebugStringA("Reached end of buffer\n");
                return lrc;
            }
            lrc ^= data;
        }
    }
};


#endif //RARE_TERMINAL_TERMINAL_HPP
