# RareTerminal
[![N|Solid](http://realmland.eu/dev/test.png)]()

RareTerminal is Win32 app that allows you to communicate with Terminal POS(Point of sale) only using *HTTP requests*.


### Devs
Building is done with **CMake**.

Currently only supporting and focusing [ECR2 protocol](docs/ecr2-protocol.pdf).