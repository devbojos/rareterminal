//
// Created by maros on 8/25/2020.
//

#include "TransPayment.hpp"

TransPayment::TransPayment(float amount, float cashbackAmount, const char *variableSymbol)
        : TransPacket(1), amount(amount), cashbackAmount(cashbackAmount), variableSymbol(variableSymbol) {}

char *TransPayment::ToBuffer() {
    char* buf = new char[250];
    sprintf(buf, R"(%cTRANS\%d\%.2f\%.2f\%s\v%d%c)",
            0x2,
            this->TRANS_ID,
            this->amount,
            this->cashbackAmount,
            this->variableSymbol,
            this->PROTOCOL_VERSION,
            0x3);
    return buf;
}
