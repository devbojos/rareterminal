//
// Created by maros on 8/24/2020.
//

#include "Terminal.hpp"
#include "Packets.hpp"

Terminal::Terminal(const char *terminalAddr, unsigned int terminalPort) {
    this->socket_family   = AF_INET;
    this->socket_type     = SOCK_STREAM;
    this->socket_protocol = IPPROTO_TCP;

    this->addr.sin_family = this->socket_family;
    this->addr.sin_port   = htons(terminalPort);
    this->addr.sin_addr
             .S_un.S_addr = inet_addr(terminalAddr);
}

bool Terminal::Connect() {

    int result;
    result = WSAStartup(MAKEWORD(2,2), &this->wsaData);
    if (result != 0) {
        printf("WinSocketAPI failed with error: 0x%X\n", result);
        return false;
    }

    sockd = socket(this->socket_family, this->socket_type, this->socket_protocol);
    if (sockd == INVALID_SOCKET) {
        int error = WSAGetLastError();
        printf("Socket construction failed with result: 0x%X\nWSA Error: 0x%X\n", result, error);
        WSACleanup();
        return false;
    } else
        printf("Socket construction successful\n");

    result = connect(sockd, (const sockaddr*)&addr, sizeof(struct sockaddr));
    if(result == SOCKET_ERROR) {
        int error = WSAGetLastError();
        if(error == WSAECONNREFUSED) {
            printf("Host refused connection.\n");
            return false;
        }
        if(error == WSAETIMEDOUT) {
            printf("Host did not properly respond.\n");
            printf("Wrong addr and/or port?");
            return false;
        }

        printf("Socket connect failed with result: 0x%X\nWSA Error: 0x%X\n", result, error);
        WSACleanup();
        return false;
    }

    const char ENQ = 0x5;
    send(sockd, &ENQ, 1, 0);

    char buf[1];
    recv(sockd, buf, 1, 0);
    printf("Got: %X\n", *buf);

    TransPayment payment(60, 0, "AAA");
    const char* pktBuf = payment.ToBuffer();
    char treatedPktBuf[250] = {0};

    sprintf(treatedPktBuf, "%s%c", pktBuf, Terminal::CalcLRC(pktBuf, 1));
    printf("%s\n", treatedPktBuf);
    send(sockd, treatedPktBuf, 250, 0);

    recv(sockd, buf, 1, 0);
    printf("Got: %X\n", *buf);

    return true;
}

bool Terminal::Disconnect() {
    this->shouldDisconnect = true;

    const char NAK = 0x15;
    send(sockd, &NAK, 1, 0);
    closesocket(this->sockd);

    return true;
}

Terminal::~Terminal() {

}


